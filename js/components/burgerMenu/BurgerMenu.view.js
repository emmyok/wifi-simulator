export default class BurgerMenuView {
    constructor(controller) {
        this._controller = controller
        this._burger = document.getElementsByClassName('burger-menu')[0]

        this._detectClick()
    }

    _detectClick() {
        this._burger.addEventListener('pointerup', () => {
            this._burger.classList.toggle('change')
            this._controller.change()
        })
    }
}
