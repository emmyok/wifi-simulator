import BurgerMenuView from './BurgerMenu.view'
import {emitter} from '../../emitter'

export default class BurgerMenuController {
    constructor() {
        this._view = new BurgerMenuView(this)
    }

    change() {
        emitter.emit('burger')
    }
}
