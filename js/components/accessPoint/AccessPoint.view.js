import {accessPointModel} from './AccessPoint.model'
import {leftPanelModel} from '../leftPanel/LeftPanel.model'
import * as pepjs from 'pepjs'

export default class AccessPointView {
    constructor(controller) {
        this._controller = controller
        this._accessPoint = document.getElementById('access-point')
        this._accessPointCoverageArea = document.getElementById('access-point-coverage-area')
        this._scale = document.getElementsByClassName('scale')[0]

        accessPointModel.setAccessPointSize(this._accessPoint.offsetWidth, this._accessPoint.offsetHeight)

        this._setDefaultPosition()
        this._detectPointerEvents()
        this._checkMiddlePosition()
    }

    _setDefaultPosition() {
        const LEFT_PANEL_SIZE = leftPanelModel.getPanelSize()
        const ACCESS_POINT_SIZE = accessPointModel.getAccessPointSize()

        const X = LEFT_PANEL_SIZE.width / 2 - ACCESS_POINT_SIZE.width / 2
        const Y = LEFT_PANEL_SIZE.height / 2 - ACCESS_POINT_SIZE.height / 2

        this._accessPoint.style.left = `${X}px`
        this._accessPoint.style.top = `${Y}px`
    }

    _getAccessPointPosition() {
        const RECT = this._accessPoint.getBoundingClientRect();

        return {
            x: RECT.left,
            y: RECT.top
        }
    }

    _setAccessPointPosition() {
        this._orgAccessPointPosition = this._getAccessPointPosition()
    }

    _move(e) {
        const X = this._orgAccessPointPosition.x - (this._pointerDownPosition.x - e.x)
        const Y = this._orgAccessPointPosition.y - (this._pointerDownPosition.y - e.y)
        this._accessPoint.style.left = `${X}px`
        this._accessPoint.style.top = `${Y}px`

        accessPointModel.savePosition({x: X, y: Y})
        this._checkMiddlePosition()
        this._controller._calculations()
    }

    _checkMiddlePosition() {
        const POSITION = this._getAccessPointPosition()
        const SIZE = accessPointModel.getAccessPointSize()
        const MIDDLE_X = POSITION.x + SIZE.width / 2
        const MIDDLE_Y = POSITION.y + SIZE.height / 2
        accessPointModel.saveMiddlePosition({x: MIDDLE_X, y: MIDDLE_Y})
    }

    _detectPointerEvents() {
        this._accessPoint.addEventListener('pointerdown', (e) => {
            e.preventDefault();
            this._isPointerDown = true
            this._setAccessPointPosition()
            this._pointerDownPosition = {
                x: e.x,
                y: e.y
            }
        });
        this._accessPoint.addEventListener('pointermove', (e) => {
            if (!this._isPointerDown) return
            this._move(e)
        });
        this._accessPoint.addEventListener('pointerup', (e) => {
            this._isPointerDown = false
        });
        this._accessPoint.addEventListener('pointerleave', (e) => {
            this._isPointerDown = false
        });
    }

    changeCoverageArea() {
        const ACCES_POINT_IMAGE_SIZE = accessPointModel.ACCES_POINT_IMAGE_SIZE
        const PX_TO_DISTANCE = accessPointModel.PX_TO_DISTANCE
        const DISTANCE_M = accessPointModel.getDistance()
        const PX = ACCES_POINT_IMAGE_SIZE + PX_TO_DISTANCE * DISTANCE_M * 2
        const HALF_PX = PX / 2

        this._accessPointCoverageArea.style.width = `${PX}px`
        this._accessPointCoverageArea.style.height = `${PX}px`
        this._accessPointCoverageArea.style.left = `calc(50% - ${HALF_PX}px)`
        this._accessPointCoverageArea.style.top = `calc(50% - ${HALF_PX}px)`

        this._scale.style.width = `${PX_TO_DISTANCE * 100}px`
    }
}
