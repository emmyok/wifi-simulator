class AccessPointModel {
    constructor() {
        this.PX_TO_DISTANCE = 1.5 // 1.5px = 1m
        this.ACCES_POINT_IMAGE_SIZE = 136
    }

    setAccessPointSize(width, height) {
        this._accessPointSize = {
            width: width,
            height: height
        }
    }

    getAccessPointSize() {
        return this._accessPointSize
    }

    saveEIRP(eirp) {
        this._eirp = eirp
    }

    getEIRP() {
        return this._eirp
    }

    savePathLoss(pathLoss) {
        this._pathLoss = pathLoss
    }

    getPathLoss() {
        return this._pathLoss
    }

    saveDistance(distance) {
        this._distance = distance
    }

    getDistance() {
        return this._distance
    }

    saveMiddlePosition(middlePosition) {
        this._middlePosition = middlePosition
    }

    getMiddlePosition() {
        return this._middlePosition
    }

    savePosition(position) {
        this._position = position
    }

    getPosition() {
        return this._position
    }
}

export let accessPointModel = new AccessPointModel();
