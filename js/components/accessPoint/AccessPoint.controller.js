import AccessPointView from './AccessPoint.view'
import {accessPointModel} from "./AccessPoint.model";
import {rightPanelModel} from '../rightPanel/RightPanel.model'
import {receiversModel} from "../receivers/Receivers.model";
import {emitter} from '../../emitter'

export default class AccessPointController {
    constructor() {
        this._view = new AccessPointView(this)

        this._calculations = this._calculations.bind(this)

        this._setListeners()
        this._calculations()
    }

    _setListeners() {
        emitter.on('save', this._calculations)
    }

    _calculations() {
        this._calculateEIRP()
        this._calculatePathLoss()
        this._calculateDistance()
        this._checkDistanceToTheReceivers()
    }

    _calculateEIRP() {
        const POWER_LOSS = 0
        const ANTENA_GAIN = 1
        const POWER = rightPanelModel.getPowerValueFromMap()

        const EIRP = POWER - POWER_LOSS + ANTENA_GAIN
        accessPointModel.saveEIRP(EIRP)
    }

    _calculatePathLoss() {
        const EIRP = accessPointModel.getEIRP()
        const SIGNAL = -80
        const PATH_LOSS = EIRP - SIGNAL

        accessPointModel.savePathLoss(PATH_LOSS)
    }

    _calculateDistance() {
        const PATH_LOSS = accessPointModel.getPathLoss()
        const F = parseFloat(rightPanelModel.getRadioValue())
        const DISTANCE = Math.floor(Math.pow(10, (PATH_LOSS - 93.45 - (20 * Math.log10(F))) / 20) * 1000)
        accessPointModel.saveDistance(DISTANCE)

        this._view.changeCoverageArea()
    }

    _checkDistanceToTheReceivers() {
        const MIDDLE_POSITION = accessPointModel.getMiddlePosition()
        const RECEIVERS = receiversModel.getPositions()
        const PX_TO_DISTANCE = accessPointModel.PX_TO_DISTANCE
        const HALF_ACCES_POINT_IMAGE_SIZE = accessPointModel.ACCES_POINT_IMAGE_SIZE / 2
        const HALF_ACCES_POINT_IMAGE_SIZE_M = HALF_ACCES_POINT_IMAGE_SIZE / PX_TO_DISTANCE
        const RECEIVER_HALF_SIZE = receiversModel.SIZE / 2

        RECEIVERS.forEach((receiver, i) => {
            const A = Math.pow(receiver.x + RECEIVER_HALF_SIZE - MIDDLE_POSITION.x, 2)
            const B = Math.pow(receiver.y + RECEIVER_HALF_SIZE - MIDDLE_POSITION.y, 2)
            const DISTANCE_PX = Math.sqrt(A + B)
            const DISTANCE_M = DISTANCE_PX / PX_TO_DISTANCE - HALF_ACCES_POINT_IMAGE_SIZE_M
            if (DISTANCE_M <= accessPointModel.getDistance()) {
                emitter.emit('receiver', null, i, true)
            } else {
                emitter.emit('receiver', null, i, false)
            }
        })
    }
}
