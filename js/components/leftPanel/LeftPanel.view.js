import {leftPanelModel} from './LeftPanel.model'

export default class LeftPanelView {
    constructor() {
        this._leftPanel = document.getElementsByClassName('left-Panel')[0]

        leftPanelModel.setPanelSize(this._leftPanel.offsetWidth, this._leftPanel.offsetHeight)
    }
}
