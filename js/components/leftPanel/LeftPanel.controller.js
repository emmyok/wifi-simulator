import LeftPanelView from './LeftPanel.view'

export default class LeftPanelController {
    constructor() {
        this._view = new LeftPanelView()
    }
}
