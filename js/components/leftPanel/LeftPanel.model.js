class LeftPanelModel {
    constructor() {

    }

    setPanelSize(width, height) {
        this._panelSize = {
            width: width,
            height: height
        }
    }

    getPanelSize() {
        return this._panelSize
    }
}

export let leftPanelModel = new LeftPanelModel();
