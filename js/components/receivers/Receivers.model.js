class ReceiversModel{
    constructor() {
        this.SIZE = 15
        this.HOW_MANY_RECEIVERS = 10
    }

    saveReceiversPositions(positions){
        this._positions = positions
    }

    getPositions(){
        return this._positions
    }
}

export let receiversModel = new ReceiversModel()
