import ReceiversView from "./Receivers.view";
import {leftPanelModel} from "../leftPanel/LeftPanel.model";
import {receiversModel} from "./Receivers.model";
import {emitter} from '../../emitter'

export default class ReceiversController {
    constructor() {
        this._view = new ReceiversView()

        this._createReceivers()

        emitter.on('receiver', this._view.changeColor.bind(this._view))
    }


    _createReceivers() {
        let receiversPositions = []
        const HOW_MANY_RECEIVERS = receiversModel.HOW_MANY_RECEIVERS
        for (let x = 0; x < HOW_MANY_RECEIVERS; x++) {
            receiversPositions.push(
                {
                    x: this._getRandomX(),
                    y: this._getRandomY()
                }
            )
        }
        receiversModel.saveReceiversPositions(receiversPositions)
        this._view.createReceivers()
    }

    _getRandomX() {
        return Math.floor(Math.random() * (leftPanelModel.getPanelSize().width - 50)) + 25
    }

    _getRandomY() {
        return Math.floor(Math.random() * (leftPanelModel.getPanelSize().height - 50)) + 25
    }
}
