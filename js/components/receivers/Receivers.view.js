import {receiversModel} from "./Receivers.model";


export default class ReceiversView {
    constructor() {
        this._receiversContainer = document.getElementById('receivers-Container')
        this._divs = []
    }

    createReceivers() {
        const POSITIONS = receiversModel.getPositions()
        POSITIONS.forEach((position, i) => {
            let div = document.createElement('div')
            div.className = 'receiver'
            div.setAttribute('style', `top:${position.y}px; left:${position.x}px`);
            this._divs.push(div)
            this._receiversContainer.appendChild(div)
        })
    }

    changeColor(param) {
        if (!this._divs || !param || param.length < 2) return

        if (param[3]) {
            this._divs[param[2]].classList.add('green-receiver')
        } else {
            this._divs[param[2]].classList.remove('green-receiver')
        }
    }
}
