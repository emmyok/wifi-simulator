class RightPanelModel {
    constructor() {
        this._radioValue = '2.4'
        this._powerValue = '0'

        this._powerValueMap = new Map()
        this._powerValueMap.set(0, 4)
        this._powerValueMap.set(1, -6)
        this._powerValueMap.set(2, -16)
    }

    setPanelSize(width, height) {
        this._panelSize = {
            width: width,
            height: height
        }
    }

    getPanelSize() {
        return this._panelSize
    }

    setRadioValue(value) {
        this._tmpRadioValue = value
    }

    getRadioValue() {
        return this._radioValue
    }

    setPowerValue(value) {
        this._tmpPowerValue = value
    }

    getPowerValue() {
        return this._powerValue
    }

    getPowerValueFromMap() {
        return this._powerValueMap.get(parseInt(this.getPowerValue()))
    }

    save() {
        if (this._tmpRadioValue) {
            this._radioValue = this._tmpRadioValue
        }
        if (this._tmpPowerValue) {
            this._powerValue = this._tmpPowerValue
        }
        this._tmpRadioValue = null
        this._tmpPowerValue = null
    }

    cancel() {
        this._tmpRadioValue = null
        this._tmpPowerValue = null
    }
}

export let rightPanelModel = new RightPanelModel();
