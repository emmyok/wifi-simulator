import RightPanelView from './RightPanel.view'
import {emitter} from '../../emitter'

export default class RightPanelController {
    constructor() {
        this._view = new RightPanelView(this)

        emitter.on('burger', () => {
            this._view.fullSize()
        })
    }

    save() {
        emitter.emit('save')
    }
}
