import {rightPanelModel} from './RightPanel.model'

export default class RightPanelView {
    constructor(controller) {
        this._controller = controller
        this._saveBtn = document.getElementsByClassName('rp-btn-save')[0]
        this._cancelBtn = document.getElementsByClassName('rp-btn-cancel')[0]
        this._rightPanel = document.getElementsByClassName('right-Panel')[0]
        this._powerList = document.getElementsByClassName('rp-powerList')[0]
        this._radioList = document.powerForm.radio


        rightPanelModel.setPanelSize(this._rightPanel.offsetWidth, this._rightPanel.offsetHeight)

        this._detectPowerChange()
        this._detectRadioChange()
        this._detectSaveOnClick()
        this._detectCancelOnClick()
    }

    _detectPowerChange() {
        this._powerList.addEventListener('change', () => {
            rightPanelModel.setPowerValue(this._powerList.value)
        })
    }

    _detectRadioChange() {
        for (let i = 0; i < this._radioList.length; i++) {
            this._radioList[i].onclick = (e) => {
                rightPanelModel.setRadioValue(e.srcElement.value)
            };
        }
    }

    _detectSaveOnClick() {
        this._saveBtn.addEventListener('click', () => {
            rightPanelModel.save()
            this._controller.save()
        })
    }

    _detectCancelOnClick() {
        this._cancelBtn.addEventListener('click', () => {
            this._radioList.forEach((input) => {
                if (input.value === rightPanelModel.getRadioValue()) {
                    input.checked = true
                }
            })

            this._powerList.options[rightPanelModel.getPowerValue()].selected = true;
            rightPanelModel.cancel()
        })
    }

    fullSize() {
        this._rightPanel.classList.toggle('right-Panel-open')
    }
}
