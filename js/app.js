import AccessPointController from './components/accessPoint/AccessPoint.controller'
import LeftPanelController from './components/leftPanel/LeftPanel.controller'
import RightPanelController from './components/rightPanel/RightPanel.controller'
import ReceiversController from './components/receivers/Receivers.controller'
import BurgerMenuController from './components/burgerMenu/BurgerMenu.controller'

class App {
    constructor() {
        this._burgerMenuController = new BurgerMenuController()
        this._leftPanelController = new LeftPanelController()
        this._receiversController = new ReceiversController()
        this._rightPanelController = new RightPanelController()
        this._accessPointController = new AccessPointController()
    }
}

window.addEventListener('load', () => {
    const app = new App()
})
