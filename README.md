# README #

Wifi Simulator is a simple web application.
Application randomly choose 10 receivers and place them on the left Panel.
Access Point is situated in the middle and it has a coverage area.
You can move the access point by drag and drop.
All receivers who are in the range area are in green otherwise red.
Application is reponsive and it is mobile friendly.
Technology used is Vanilla JavaScript (ES2016), WebPack, MVC, OOP.

### How do I get set up? ###

* npm install
* npm start
