const path = require('path')

module.exports = {
  entry: './js/app.js',
  output: {
    path: path.resolve(__dirname, 'assets'),
    filename: 'app.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, 'assets'),
    compress: true,
    port: 8080
  }
}
